<?php

use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([trans('general.male'), trans('general.female')] as $value) {

            \Illuminate\Support\Facades\DB::table('genders')->insert([
                'name' => $value
            ]);
        }
    }
}
