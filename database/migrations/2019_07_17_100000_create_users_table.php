<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('required_password_change')->default(0);
            $table->tinyInteger('is_super_admin')->default(0);
            $table->integer('office_id')->unsigned()->nullable();
            $table->string('photo')->nullable();
            $table->string('first_mobile')->nullable();
            $table->string('second_mobile')->nullable();
            $table->string('address')->nullable();
            $table->string('job')->nullable();
            $table->integer('gender_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('office_id')->references('id')->on('offices')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('gender_id')->references('id')->on('genders')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
