<?php

return [
    'add_success' => 'تمت الإضافة بنجاح',
    'update_success' => 'تم التعديل بنجاح',
    'delete_success' => 'تم الحذف بنجاح',
    'photo_upload_error' => 'خطأ فى رفع الصورة',
    'office_already_has_user' => 'المكتب الفرعى المختار مرتبط بمستخدم بالفعل',
    'cannot_delete_super_admin' => 'لا يمكن حذف مدير النظام',
    'date_must_be_within_month' => 'هذا التاريخ لابد ان يكون فى خلال رقم الشهر المختار شهر :month',
    'date_must_be_within_year' => 'هذا التاريخ لابد ان يكون فى خلال السنة المالية المختارة سنة :year',
    'this_month_duplicated' => 'رقم الشهر :month مكرر',
    'this_year_already_exist' => 'هذه السنة المالية موجودة بالفعل فى قاعدة البيانات',
    'this_month_already_exist' => 'هذا الشهر بتاريخ البداية والنهاية موجودان فى قاعدة البيانات',
    'profile_updated' => 'تم تعديل الملف الشخصى',
    'same_password_error' => 'برجاء اختيار كلمة مرور مختلفة عن كلمة المرور الحالية',
    'password_changed' => 'تم تعديل كلمة المرور',
    'cannot_delete_office' => 'لا يمكن حذف المكتب لوجود ايرادات او مستخدين مرتبطين به',
    'cannot_delete_user' => 'لا يمكن حذف المستخدم لوجود ايرادات مرتبطة به'
];