<?php

return [
  'male' => 'ذكر',
  'female' => 'أنثى',
  'confirm_delete' => 'تأكيد الحذف',
  'id' => 'الرقم التسلسلى',
   'home' => 'الرئيسية',
   'control_panel' => 'لوحة التحكم',
    'office' => 'مكتب فرعى',
    'system_admin' => 'مدير النظام',
    'logout' => 'خروج',
    'profile' => 'ملفى الشخصى',
    'password_change_required' => 'برجاء تغيير كلمة المرور!',
    'edit_profile' => 'تعديل الملف الشخصى',
    'change_password' => 'تعديل كلمة المرور',
    'save' => 'حفظ',
    'latest_added_revenues' => 'الإيرادات المضافة مؤخرا',
    'view_all' => 'استعراض كل الإيرادات',
    'my_added_revenues' => 'الايرادات التى اضفتها مؤخرا'
];