<?php

return [
  'months' => 'الشهور',
  'month_singular' => 'الشهر',
  'year' => 'السنة المالية',
  'month' => 'رقم الشهر',
    'start_date' => 'تاريخ البداية',
    'end_date' => 'تاريخ النهاية',
    'create_new_month' => 'إضافة شهر جديد',
    'edit_month' => 'تعديل شهر'
];