<?php

return [
    'users' => 'إدارة المستخدمين',
    'user_singular' => 'مستخدم',
    'name' => 'الإسم',
    'email' => 'البريد الإلكترونى',
    'password' => 'كلمة المرور',
    'is_super_admin' => 'مدير النظام',
    'office' => 'المكتب الفرعى',
    'photo' => 'الصورة',
    'first_mobile' => 'الموبايل الأول',
    'second_mobile' => 'الموبايل الثانى',
    'address' => 'العنوان',
    'job' => 'الوظيفة',
    'gender' => 'الجنس',
    'user_revenues' => 'إيرادات المستخدم',
    'create_new_user' => 'إضافة مستخدم جديد',
    'edit_user' => 'تعديل مستخدم',
    'password_confirmation' => 'تأكيد كلمة المرور'
];