<?php

return [
    'offices' => 'المكاتب الفرعية',
    'office_singular' => 'المكتب الفرعى',
    'create_new_office' => 'إضافة مكتب جديد',
    'edit_office' => 'تعديل المكتب الفرعى',
    'name' => 'إسم المكتب الفرعى',
    'address' => 'عنوان المكتب ',
    'description' => 'وصف المكتب '
];