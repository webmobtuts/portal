<?php

return [
    'revenues' => 'الإيرادات',
    'revenue_singular' => 'إيراد',
    'amount' => 'مبلغ الإيراد',
    'month' => 'عن شهر',
    'year' => 'السنة المالية',
    'office' => 'مكتب فرعى',
    'user' => 'بواسطة مستخدم',
    'description' => 'سبب التوريد',
    'create_new_revenue' => 'توريد مبلغ',
    'edit_revenue' => 'تعديل توريد'
];