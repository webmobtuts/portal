<?php

return [
    'add_new' => 'أضف جديد',
    'search' => 'إبحث',
    'search_name_email' => 'ابحث بالاسم او البريد الالكترونى',
    'back' => 'رجوع',
    'create' => 'إضافة',
    'update' => 'تحديث',
    'view' => 'عرض',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'actions' => 'خيارات',
    'select_office' => 'اختر المكتب الفرعى',
    'select_year' => 'اختر السنة المالية',
    'select_month' => 'اختر الشهر',
    'select_user' => 'اختر المستخدم',
    'total' => 'الإجمالى'
];