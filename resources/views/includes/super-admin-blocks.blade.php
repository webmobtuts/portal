<div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-building"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans('sidebar.offices') }}</span>
                <span class="info-box-number">{{ getOfficesCount() }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans('sidebar.userss') }}</span>
                <span class="info-box-number">{{ getUsersCount() }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans('sidebar.revenues') }}</span>
                <span class="info-box-number">{{ getTotalRevenues() }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('general.latest_added_revenues') }}</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th>{{ trans('general.id') }}</th>
                                <th>{{ trans('modules/revenues.amount') }}</th>
                                <th>{{ trans('modules/revenues.user') }}</th>
                                <th>{{ trans('modules/revenues.office') }}</th>
                                <th>{{ trans('modules/revenues.year') }}</th>
                                <th>{{ trans('modules/revenues.month') }}</th>
                                <th>{{ trans('labels.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(getLatestRevenues() as $item)
                                <tr>
                                    <td><a href="{{ url('/admin/revenues/' . $item->id) }}">{{ $item->id }}</a></td>
                                    <td>{{ $item->amount }}</td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>{!! isset($item->office->name)?$item->office->name:'<span class="label label-warning">لا يوجد</span>' !!}</td>
                                    <td>{{ $item->month->year }}</td>
                                    <td>{{ $item->month->month }}</td>
                                    <td>
                                        <a href="{{ url('/admin/revenues/' . $item->id) }}" title="{{ trans('labels.view') }}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="{{ url('admin/revenues') }}" class="btn btn-sm btn-default btn-flat pull-right">{{ trans('general.view_all') }}</a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>

</div>