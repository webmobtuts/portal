<div class="row">

    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('general.my_added_revenues') }}</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>{{ trans('general.id') }}</th>
                            <th>{{ trans('modules/revenues.amount') }}</th>
                            <th>{{ trans('modules/revenues.year') }}</th>
                            <th>{{ trans('modules/revenues.month') }}</th>
                            <th>{{ trans('labels.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(getLatestRevenues(\Auth::user()->id) as $item)
                            <tr>
                                <td><a href="{{ url('/admin/revenues/' . $item->id) }}">{{ $item->id }}</a></td>
                                <td>{{ $item->amount }}</td>
                                <td>{{ $item->month->year }}</td>
                                <td>{{ $item->month->month }}</td>
                                <td>
                                    <a href="{{ url('/admin/revenues/' . $item->id) }}" title="{{ trans('labels.view') }}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="{{ url('admin/revenues') }}" class="btn btn-sm btn-default btn-flat pull-right">{{ trans('general.view_all') }}</a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>

</div>