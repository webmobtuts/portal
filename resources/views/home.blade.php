@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ trans('general.home') }}
            <small>{{ trans('general.control_panel') }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> {{ trans('general.home') }}</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        @if(\Auth::user()->is_super_admin == 1)
            @include('includes.super-admin-blocks')
        @else
            @include('includes.normal-user-blocks')
        @endif

    </section>

@endsection
