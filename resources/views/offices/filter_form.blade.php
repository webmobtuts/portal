<div class="panel panel-info">
    <div class="panel-heading">
        {{ trans('labels.search') }}
        <div class="pull-right">
            <a class="btn btn-success btn-sm pull-right" href="{{ url('/admin/offices/create') }}" title="{{ trans('labels.add_new') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('labels.add_new') }}
            </a>
        </div>
    </div>
    <div class="panel-body">

        <form class="form-inline" method="GET" action="{{ url('/admin/offices') }}">
            <div class="form-group">
                <input class="form-control" name="search" placeholder="{{ trans('labels.search') }}" type="text" value="{{ request('search') }}">
            </div>
            <div class="form-group">
                <label>{{ trans('general.id') }}</label>
                <input class="form-control input-sm" name="id" placeholder="{{ trans('general.id') }}" type="text" value="{{ request('id') }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>

</div>
