@extends('layouts.app')

@section('title', ' | ' . trans('modules/offices.offices'))

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/offices.offices') }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li class="active">Offices</li>--}}
        {{--</ol>--}}
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @include('includes.flash_message')

                        <div class="row">
                            <div class="col-md-12">
                                @include('offices.filter_form')
                            </div>
                        </div>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>{{ trans('general.id') }}</th>
                                    <th>{{ trans('modules/offices.name') }}</th>
                                    <th>{{ trans('modules/offices.address') }}</th>
                                    <th>{{ trans('modules/offices.description') }}</th>
                                    <th>{{ trans('labels.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($offices as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td><td>{{ $item->address }}</td><td>{{ $item->description }}</td>
                                            <td>
                                                <a href="{{ url('/admin/offices/' . $item->id) }}" title="{{ trans('labels.view') }}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('labels.view') }}</button></a>
                                                <a href="{{ url('/admin/offices/' . $item->id . '/edit') }}" title="{{ trans('labels.edit') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>

                                                <form method="POST" action="{{ url('/admin/offices' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.actions') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $offices->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
