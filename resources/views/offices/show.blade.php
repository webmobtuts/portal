@extends('layouts.app')

@section('title', ' | ' . trans('modules/offices.office_singular') . ' #' . $office->id)

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/offices.office_singular') }} #{{ $office->id }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li><a href="{{ url('/admin/offices') }}">Offices</a></li>--}}
            {{--<li class="active">Show</li>--}}
        {{--</ol>--}}
    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <a href="{{ url('/admin/offices') }}" title="{{ trans('labels.back') }}"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('labels.back') }}</button></a>
                        <a href="{{ url('/admin/offices/' . $office->id . '/edit') }}" title="Edit Office"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>

                        <form method="POST" action="{{ url('admin/offices' . '/' . $office->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.delete') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>{{ trans('general.id') }}</th><td>{{ $office->id }}</td>
                                    </tr>
                                    <tr><th> {{ trans('modules/offices.name') }} </th><td> {{ $office->name }} </td></tr>
                                    <tr><th> {{ trans('modules/offices.address') }} </th><td> {{ $office->address }} </td></tr>
                                    <tr><th> {{ trans('modules/offices.description') }} </th><td> {{ $office->description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
