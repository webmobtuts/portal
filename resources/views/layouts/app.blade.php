<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    @include('layouts.partials.styles')

    <script>
        var BASE_URL = '{{ url("/") }}';
    </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    @include('layouts.partials.header')

    @include('layouts.partials.sidebar')

    <div class="content-wrapper">

        @if(\Auth::user()->required_password_change == 1)
            <div class="callout callout-warning">
                <h4>{{ trans('general.password_change_required') }}</h4>
            </div>
        @endif

        @yield('content')
    </div>

    @include('layouts.partials.footer')
</div>


    @include('layouts.partials.scripts')
</body>
</html>
