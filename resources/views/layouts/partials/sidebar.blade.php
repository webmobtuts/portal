<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ getUserPhoto() }}" class="img-circle" style="height: 45px;" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ \Auth::user()->name }}</p>
                <a href="#">
                    {{ (\Auth::user()->is_super_admin==1?trans('general.system_admin'):\Auth::user()->office->name) }}
                </a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">{{ trans('sidebar.main_nav') }}</li>

            @foreach(getModules() as $module)

                @if(!isset($module['subitems']))

                    @if($module['access'] == true)
                        <li class="{{ $module['active_class']  }}">

                            <a href="{{ $module['url'] }}"><i class="{{ $module['icon'] }}"></i> {{ $module['title'] }}</a>

                        </li>
                    @endif

                @else
                    @if($module['access'] == true)
                        <li class="{{ $module['active_class'] . " treeview"  }}">

                            <a href="#">
                                <i class="{{ $module['icon'] }}"></i> <span>{{ $module['title'] }}</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>

                            <ul class="treeview-menu">

                                @foreach($module['subitems'] as $subitem)
                                    @if($subitem['access'] == true)
                                        <li class="{{ $subitem['active_class'] }}"><a href="{{ $subitem['url'] }}"><i class="{{ $subitem['icon'] }}"></i> {{ $subitem['title'] }}</a></li>
                                    @endif
                                @endforeach

                            </ul>

                        </li>
                    @endif
                @endif

            @endforeach
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>