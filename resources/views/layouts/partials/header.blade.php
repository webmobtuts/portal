<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/admin') }}" class="logo">
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>PORT</b>AL</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <img src="{{ getUserPhoto() }}" width="160" height="160" class="user-image" alt="User Image">

                        <span class="hidden-xs">{{ \Auth::user()->name . ' - ' . (\Auth::user()->is_super_admin==1?trans('general.system_admin'):trans('general.office') . ": " . \Auth::user()->office->name) }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">

                            <img src="{{ getUserPhoto() }}" width="160" height="160" class="img-circle" alt="User Image">

                            <p>
                                {{ \Auth::user()->name . ' - ' . (\Auth::user()->is_super_admin==1?trans('general.system_admin'):trans('general.office') . ": " . \Auth::user()->office->name) }}
                                @if(\Auth::user()->created_at != null) <small>Member since {{ \Auth::user()->created_at->diffForHumans() }}</small> @endif
                            </p>

                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('admin/my-profile') }}" class="btn btn-default btn-flat">{{ trans('general.profile') }}</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">{{ trans('general.logout') }}</a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>