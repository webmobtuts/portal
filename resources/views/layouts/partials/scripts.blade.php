
<!-- jQuery 3 -->
<script src="{{ asset('theme/bower_components') }}/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('theme/bower_components') }}/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
{{--<script src="{{ asset('theme/bower_components') }}/bootstrap/dist/js/bootstrap.min.js"></script>--}}

<script
        src="https://cdn.rtlcss.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-B4D+9otHJ5PJZQbqWyDHJc6z6st5fX3r680CYa0Em9AUG6jqu5t473Y+1CTZQWZv"
        crossorigin="anonymous"></script>

<!-- daterangepicker -->
<script src="{{ asset('theme/bower_components') }}/moment/min/moment.min.js"></script>
<script src="{{ asset('theme/bower_components') }}/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="{{ asset('theme/bower_components') }}/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('theme/plugins') }}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('theme/bower_components') }}/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('theme/dist') }}/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('theme/dist') }}/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('theme/dist') }}/js/demo.js"></script>

@yield('scripts')