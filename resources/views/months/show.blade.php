@extends('layouts.app')

@section('title', ' | ' . trans('modules/months.month_singular') . ' #' . $month->id)

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/months.month_singular') }} #{{ $month->id }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li><a href="{{ url('/admin/months') }}">Months</a></li>--}}
            {{--<li class="active">Show</li>--}}
        {{--</ol>--}}
    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <a href="{{ url('/admin/months') }}" title="{{ trans('labels.back') }}"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('labels.back') }}</button></a>
                        {{--<a href="{{ url('/admin/months/' . $month->id . '/edit') }}" title="{{ trans('labels.edit') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>--}}

                        {{--<form method="POST" action="{{ url('admin/months' . '/' . $month->id) }}" accept-charset="UTF-8" style="display:inline">--}}
                            {{--{{ method_field('DELETE') }}--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.delete') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>--}}
                        {{--</form>--}}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>{{ trans('general.id') }}</th><td>{{ $month->id }}</td>
                                    </tr>
                                    <tr><th> {{ trans('modules/months.month') }} </th><td> {{ $month->month }} </td></tr>
                                    <tr><th> {{ trans('modules/months.year') }} </th><td> {{ $month->year }} </td></tr>
                                    <tr><th> {{ trans('modules/months.start_date') }} </th><td> {{ $month->start_date }} </td></tr>
                                    <tr><th> {{ trans('modules/months.end_date') }} </th><td> {{ $month->end_date }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
