@extends('layouts.app')

@section('title', ' | ' . trans('modules/months.months'))

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/months.months') }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li class="active">Months</li>--}}
        {{--</ol>--}}
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @include('includes.flash_message')

                        <div class="row">
                            <div class="col-md-12">
                                @include('months.filter_form')
                            </div>
                        </div>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>{{ trans('general.id') }}</th>
                                    <th>{{ trans('modules/months.month') }}</th>
                                    <th>{{ trans('modules/months.year') }}</th>
                                    <th>{{ trans('modules/months.start_date') }}</th>
                                    <th>{{ trans('modules/months.end_date') }}</th>
                                    <th>{{ trans('labels.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($months as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->month }}</td>
                                            <td>{{ $item->year }}</td>
                                            <td>{{ $item->start_date }}</td>
                                            <td>{{ $item->end_date }}</td>
                                            <td>
                                                <a href="{{ url('/admin/months/' . $item->id) }}" title="{{ trans('labels.view') }}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('labels.view') }}</button></a>
                                                {{--<a href="{{ url('/admin/months/' . $item->id . '/edit') }}" title="{{ trans('labels.edit') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>--}}

                                                {{--<form method="POST" action="{{ url('/admin/months' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">--}}
                                                    {{--{{ method_field('DELETE') }}--}}
                                                    {{--{{ csrf_field() }}--}}
                                                    {{--<button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.delete') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>--}}
                                                {{--</form>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $months->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
