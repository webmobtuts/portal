@extends('layouts.app')

@section('title', ' | ' . trans('modules/months.create_new_month'))

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/months.create_new_month') }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li><a href="{{ url('/admin/months') }}">Months</a></li>--}}
            {{--<li class="active">Create</li>--}}
        {{--</ol>--}}
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ url('/admin/months') }}" title="{{ trans('labels.back') }}"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('labels.back') }}</button></a>
                        <br />
                        <br />

                        {{--@if ($errors->any())--}}
                            {{--<ul class="alert alert-danger">--}}
                                {{--@foreach ($errors->all() as $error)--}}
                                    {{--<li>{{ $error }}</li>--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--@endif--}}

                        @include('includes.flash_message')

                        <form method="POST" action="{{ url('/admin/months') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('months.form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

    <script type="text/javascript">
        $(function () {
            $(".start_date, .end_date").datepicker({
                format: 'yyyy-mm-dd'
            });
        });
    </script>

@stop