
<div class="form-group {{ $errors->has('year') ? 'has-error' : ''}}">
    <label for="year" class="control-label">{{ trans('modules/months.year') }}</label>
    <input class="form-control" name="year" type="text" id="year" value="{{ old('year')!=null?old('year'):date('Y') }}" >
    {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
</div>

@foreach(range(1, 12) as $value)
    <div class="row">
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('month.' . $value) ? 'has-error' : ''}}">
                <label for="month-{{$value}}" class="control-label">{{ trans('modules/months.month') }}</label>
                <input class="form-control" name="month[{{$value}}]" type="text" id="month-{{$value}}" value="{{ isset($month->month) ? $month->month : old('month')[$value]}}" >
                {!! $errors->first('month.' . $value, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('start_date.' . $value) ? 'has-error' : ''}}">
                <label for="start_date-{{$value}}" class="control-label">{{ trans('modules/months.start_date') }}</label>
                <input class="form-control start_date" name="start_date[{{$value}}]" type="text" id="start_date-{{$value}}" value="{{ isset($month->start_date) ? $month->start_date : old('start_date')[$value]}}" >
                {!! $errors->first('start_date.' . $value, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('end_date.' . $value) ? 'has-error' : ''}}">
                <label for="end_date-{{$value}}" class="control-label">{{ trans('modules/months.end_date') }}</label>
                <input class="form-control end_date" name="end_date[{{$value}}]" type="text" id="end_date-{{$value}}" value="{{ isset($month->end_date) ? $month->end_date : old('end_date')[$value]}}" >
                {!! $errors->first('end_date.' . $value, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
@endforeach


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? trans('labels.update') : trans('labels.create') }}">
</div>
