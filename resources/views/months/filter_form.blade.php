<div class="panel panel-info">
    <div class="panel-heading">
        {{ trans('labels.search') }}
        <div class="pull-right">
            <a class="btn btn-success btn-sm pull-right" href="{{ url('/admin/months/create') }}" title="{{ trans('labels.add_new') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('labels.add_new') }}
            </a>
        </div>
    </div>
    <div class="panel-body">

        <form class="form-inline" method="GET" action="{{ url('/admin/months') }}">
            <div class="form-group">
                <label>{{ trans('general.id') }}</label>
                <input class="form-control input-sm" name="id" placeholder="{{ trans('general.id') }}" type="text" value="{{ request('id') }}">
            </div>
            <div class="form-group">
                <label>{{ trans('modules/months.year') }}</label>
                <input class="form-control input-sm" name="year" placeholder="{{ trans('modules/months.year') }}" type="text" value="{{ request('year') }}">
            </div>
            <div class="form-group">
                <label>{{ trans('modules/months.month') }}</label>
                <input class="form-control input-sm" name="month" placeholder="{{ trans('modules/months.month') }}" type="text" value="{{ request('month') }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>

</div>
