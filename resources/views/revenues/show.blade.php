@extends('layouts.app')

@section('title', ' | ' . trans('modules/revenues.revenue_singular') . ' #' . $revenue->id)

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/revenues.revenue_singular') }} #{{ $revenue->id }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li><a href="{{ url('/admin/revenues') }}">Revenues</a></li>--}}
            {{--<li class="active">Show</li>--}}
        {{--</ol>--}}
    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <a href="{{ url('/admin/revenues') }}" title="{{ trans('labels.back') }}"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('labels.back') }}</button></a>
                        <a href="{{ url('/admin/revenues/' . $revenue->id . '/edit') }}" title="{{ trans('labels.edit') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>

                        <form method="POST" action="{{ url('admin/revenues' . '/' . $revenue->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.delete') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>{{ trans('general.id') }}</th><td>{{ $revenue->id }}</td>
                                    </tr>
                                    <tr><th> {{ trans('modules/revenues.amount') }} </th><td> {{ $revenue->amount }} </td></tr>
                                    @if(\Auth::user()->is_super_admin == 1)
                                        <tr><th> {{ trans('modules/revenues.user') }} </th><td> {{ $revenue->user->name }} </td></tr>
                                        <tr><th> {{ trans('modules/revenues.office') }} </th><td> {!! isset($revenue->office->name)?$revenue->office->name:'<span class="label label-warning">لا يوجد</span>' !!} </td></tr>
                                    @endif
                                    <tr><th> {{ trans('modules/revenues.year') }} </th><td> {{ $revenue->month->year }} </td></tr>
                                    <tr><th> {{ trans('modules/revenues.month') }} </th><td> {{ $revenue->month->month }} </td></tr>
                                    <tr><th> {{ trans('modules/revenues.description') }} </th><td> {{ $revenue->description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
