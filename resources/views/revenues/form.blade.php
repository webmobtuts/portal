<div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
    <label for="amount" class="control-label">{{ trans('modules/revenues.amount') }}</label>
    <input class="form-control" name="amount" type="text" id="amount" value="{{ isset($revenue->amount) ? $revenue->amount : old('amount') }}" >
    {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('year') ? 'has-error' : ''}}">
            <label for="year" class="control-label">{{ trans('modules/months.year') }}</label>
            <select class="form-control" name="year" id="year">
                <option value="">{{ trans('labels.select_year') }}</option>
                @foreach($years as $year)
                    <option value="{{ $year }}" {{ isset($selectedYear)&&$selectedYear==$year?'selected':(old('year')==$year?'selected':'') }}>{{ $year }}</option>
                @endforeach
            </select>

            {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('month_id') ? 'has-error' : ''}}">
            <label for="month_id" class="control-label">{{ trans('modules/revenues.month') }}</label>
            <select class="form-control" name="month_id" id="month_id" data-selected="{{ isset($revenue->month_id) ? $revenue->month_id : old('month_id') }}">
                <option value="">{{ trans('labels.select_month') }}</option>
            </select>

            {!! $errors->first('month_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ trans('modules/revenues.description') }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($revenue->description) ? $revenue->description : old('description') }}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? trans('labels.update') : trans('labels.create') }}">
</div>
