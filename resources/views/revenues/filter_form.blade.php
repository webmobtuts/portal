<div class="panel panel-info">
    <div class="panel-heading">
        {{ trans('labels.search') }}
        <div class="pull-right">
            <a class="btn btn-success btn-sm pull-right" href="{{ url('/admin/revenues/create') }}" title="{{ trans('labels.add_new') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('labels.add_new') }}
            </a>
        </div>
    </div>
    <div class="panel-body">

        <form class="form-inline" method="GET" action="{{ url('/admin/revenues') }}">
            <div class="form-group">
                <label>{{ trans('general.id') }}</label>
                <input class="form-control input-sm" name="id" placeholder="{{ trans('general.id') }}" type="text" value="{{ request('id') }}">
            </div>

            @if(\Auth::user()->is_super_admin == 1)
                <div class="form-group">
                    <select name="user_id" id="user_id" class="form-control input-sm">
                        <option value="">{{ trans('labels.select_user') }}</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}" {{ request('user_id')==$user->id?'selected':'' }}>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select name="office_id" id="office_id" class="form-control input-sm">
                        <option value="">{{ trans('labels.select_office') }}</option>
                        @foreach($offices as $office)
                            <option value="{{ $office->id }}" {{ request('office_id')==$office->id?'selected':'' }}>{{ $office->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="form-group">
                <select name="year" id="year" class="form-control input-sm">
                    <option value="">{{ trans('labels.select_year') }}</option>
                    @foreach($years as $year)
                        <option value="{{ $year }}" {{ request('year')==$year?'selected':'' }}>{{ $year }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select name="month_id" id="month_id" class="form-control input-sm" data-selected="{{ request('month_id') }}">
                    <option value="">{{ trans('labels.select_month') }}</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>

</div>
