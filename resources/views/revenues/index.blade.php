@extends('layouts.app')

@section('title', ' | ' . trans('modules/revenues.revenues'))

@section('styles')
    <style>
        #total {
            text-align:right;
            border: 1px solid #000;
            background: #ddd;
        }
    </style>
@stop

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/revenues.revenues') }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li class="active">Revenues</li>--}}
        {{--</ol>--}}
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @include('includes.flash_message')

                        <div class="row">
                            <div class="col-md-12">
                                @include('revenues.filter_form')
                            </div>
                        </div>

                        <br/>
                        <br/>

                        @php $total = 0; @endphp

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>{{ trans('general.id') }}</th>
                                    <th>{{ trans('modules/revenues.amount') }}</th>
                                    @if(\Auth::user()->is_super_admin == 1)
                                        <th>{{ trans('modules/revenues.user') }}</th>
                                        <th>{{ trans('modules/revenues.office') }}</th>
                                    @endif
                                    <th>{{ trans('modules/revenues.year') }}</th>
                                    <th>{{ trans('modules/revenues.month') }}</th>
                                    <th>{{ trans('labels.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($revenues as $item)
                                        @php $total += $item->amount; @endphp
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->amount }}</td>
                                            @if(\Auth::user()->is_super_admin == 1)
                                                <td>{{ $item->user->name }}</td>
                                                <td>{!! isset($item->office->name)?$item->office->name:'<span class="label label-warning">لا يوجد</span>' !!}</td>
                                            @endif
                                            <td>{{ $item->month->year }}</td>
                                            <td>{{ $item->month->month }}</td>
                                            <td>
                                                <a href="{{ url('/admin/revenues/' . $item->id) }}" title="{{ trans('labels.view') }}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('labels.view') }}</button></a>
                                                <a href="{{ url('/admin/revenues/' . $item->id . '/edit') }}" title="{{ trans('labels.edit') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>

                                                <form method="POST" action="{{ url('/admin/revenues' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.delete') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                @if($total > 0)
                                    <tfoot>
                                        <tr>
                                            <th>{{ trans('labels.total') }}</th>
                                            <td id="total">{{ $total }}</td>
                                            <th colspan="5"></th>
                                        </tr>
                                    </tfoot>
                                @endif
                            </table>
                            <div class="pagination-wrapper"> {!! $revenues->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/views/revenues/functions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/views/revenues/index.js') }}"></script>
@stop