@extends('layouts.app')

@section('title', ' | ' . trans('modules/revenues.edit_revenue') . ' #' . $revenue->id)

@section('content')


    <section class="content-header">
        <h1>
            {{ trans('modules/revenues.edit_revenue') }} #{{ $revenue->id }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li><a href="{{ url('/admin/revenues') }}">Revenues</a></li>--}}
            {{--<li class="active">Edit</li>--}}
        {{--</ol>--}}
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ url('/admin/revenues') }}" title="{{ trans('labels.back') }}"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('labels.back') }}</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        @include('includes.flash_message')

                        <form method="POST" action="{{ url('/admin/revenues/' . $revenue->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('revenues.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/views/revenues/functions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/views/revenues/form.js') }}"></script>
@stop