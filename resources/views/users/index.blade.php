@extends('layouts.app')

@section('title', ' | ' . trans('modules/users.users'))

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/users.users') }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li class="active">Users</li>--}}
        {{--</ol>--}}
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @include('includes.flash_message')

                        {{--<a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm pull-right" title="{{ trans('labels.add_new') }}">--}}
                            {{--<i class="fa fa-plus" aria-hidden="true"></i> {{ trans('labels.add_new') }}--}}
                        {{--</a>--}}

                        {{--<form method="GET" action="{{ url('/admin/users') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0" role="search">--}}
                            {{--<div class="input-group">--}}
                                {{--<input type="text" class="form-control" name="search" placeholder="{{ trans('labels.search') }}..." value="{{ request('search') }}">--}}
                                {{--<span class="input-group-btn">--}}
                                    {{--<button class="btn btn-secondary" type="submit">--}}
                                        {{--<i class="fa fa-search"></i>--}}
                                    {{--</button>--}}
                                {{--</span>--}}
                            {{--</div>--}}
                        {{--</form>--}}

                        <div class="row">
                            <div class="col-md-12">
                                @include('users.filter_form')
                            </div>
                        </div>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>{{ trans('general.id') }}</th>
                                    <th>{{ trans('modules/users.name') }}</th>
                                    <th>{{ trans('modules/users.email') }}</th>
                                    <th>{{ trans('modules/users.is_super_admin') }}</th>
                                    <th>{{ trans('modules/users.office') }}</th>
                                    <th>{{ trans('modules/users.first_mobile') }}</th>
                                    <th>{{ trans('modules/users.second_mobile') }}</th>
                                    <th>{{ trans('modules/users.user_revenues') }}</th>
                                    <th>{{ trans('labels.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{!! $item->is_super_admin==1?'<span class="label label-success">نعم</span>':'<span class="label label-default">لا</span>' !!}</td>
                                            <td>{{ isset($item->office->name)?$item->office->name:"" }}</td>
                                            <td>{{ $item->first_mobile }}</td>
                                            <td>{{ $item->second_mobile }}</td>
                                            <td>{{ $item->revenues->sum('amount') }}</td>
                                            <td>
                                                <a href="{{ url('/admin/users/' . $item->id) }}" title="{{ trans('labels.view') }}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('labels.view') }}</button></a>
                                                <a href="{{ url('/admin/users/' . $item->id . '/edit') }}" title="{{ trans('labels.edit') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>


                                                @if($item->is_super_admin == 0)
                                                    <form method="POST" action="{{ url('/admin/users' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.delete') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
