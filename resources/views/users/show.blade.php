@extends('layouts.app')

@section('title', ' | ' . trans('modules/users.user_singular') . ' #' . $user->id)

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('modules/users.user_singular') }} #{{ $user->id }}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{ url('/admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>--}}
            {{--<li><a href="{{ url('/admin/users') }}">Users</a></li>--}}
            {{--<li class="active">Show</li>--}}
        {{--</ol>--}}
    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @include('includes.flash_message')

                        <a href="{{ url('/admin/users') }}" title="{{ trans('labels.back') }}"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('labels.back') }}</button></a>
                        <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="{{ trans('labels.edit') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('labels.edit') }}</button></a>

                        @if($user->is_super_admin == 0)
                            <form method="POST" action="{{ url('admin/users' . '/' . $user->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="{{ trans('labels.delete') }}" onclick="return confirm('{{ trans('general.confirm_delete') }}')"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ trans('labels.delete') }}</button>
                            </form>
                        @endif
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>

                                   @if(!empty($user->photo))
                                        <tr>
                                            <td>
                                                <img src="{{ url('uploads/users/' . $user->photo) }}" class="pull-right" width="200" height="200" />
                                            </td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <th>{{ trans('general.id') }}</th><td>{{ $user->id }}</td>
                                    </tr>
                                    <tr><th> {{ trans('modules/users.name') }} </th><td> {{ $user->name }} </td></tr>
                                    <tr><th> {{ trans('modules/users.email') }} </th><td> {{ $user->email }} </td></tr>
                                    <tr><th> {{ trans('modules/users.is_super_admin') }} </th><td> {!! $user->is_super_admin==1?'<span class="label label-success">نعم</span>':'<span class="label label-default">لا</span>' !!} </td></tr>
                                    <tr><th> {{ trans('modules/users.office') }} </th><td> {{ isset($user->office->name)?$user->office->name:"" }} </td></tr>
                                    <tr><th> {{ trans('modules/users.first_mobile') }} </th><td> {{ $user->first_mobile }} </td></tr>
                                   <tr><th> {{ trans('modules/users.second_mobile') }} </th><td> {{ $user->second_mobile }} </td></tr>
                                   <tr><th> {{ trans('modules/users.address') }} </th><td> {{ $user->address }} </td></tr>
                                   <tr><th> {{ trans('modules/users.job') }} </th><td> {{ $user->job }} </td></tr>
                                   <tr><th> {{ trans('modules/users.gender') }} </th><td> {{ isset($user->gender->name)?$user->gender->name:'' }} </td></tr>
                                   <tr><th> {{ trans('modules/users.user_revenues') }} </th><td> {{ $user->revenues->sum('amount') }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
