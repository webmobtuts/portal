@extends('layouts.app')

@section('title', ' | ' . trans('general.edit_profile'))

@section('content')


    <section class="content-header">
        <h1>
            {{ trans('general.edit_profile') }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/admin/my-profile/edit/') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                <label for="name" class="control-label">{{ trans('modules/users.name') }}</label>
                                <input class="form-control" name="name" type="text" id="name" value="{{ isset($user->name) ? $user->name : old('name')}}" >
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                <label for="email" class="control-label">{{ trans('modules/users.email') }}</label>
                                <input class="form-control" name="email" type="text" id="email" value="{{ isset($user->email) ? $user->email : old('email')}}" >
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>

                            @if(!empty($user->photo))
                                <img src="{{ url('uploads/users/' . $user->photo) }}" width="200" height="180"/>
                            @endif

                            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                                <label for="photo" class="control-label">{{ trans('modules/users.photo') }}</label>
                                <input class="form-control" name="photo" type="file" id="photo" />
                                {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('first_mobile') ? 'has-error' : ''}}">
                                        <label for="first_mobile" class="control-label">{{ trans('modules/users.first_mobile') }}</label>
                                        <input class="form-control" name="first_mobile" type="text" id="first_mobile" value="{{ isset($user->first_mobile) ? $user->first_mobile : old('first_mobile')}}" >
                                        {!! $errors->first('first_mobile', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('second_mobile') ? 'has-error' : ''}}">
                                        <label for="second_mobile" class="control-label">{{ trans('modules/users.second_mobile') }}</label>
                                        <input class="form-control" name="second_mobile" type="text" id="second_mobile" value="{{ isset($user->second_mobile) ? $user->second_mobile : old('second_mobile')}}" >
                                        {!! $errors->first('second_mobile', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                <label for="address" class="control-label">{{ trans('modules/users.address') }}</label>
                                <input type="text" class="form-control" name="address" id="address" value="{{ isset($user->address) ? $user->address : old('address')}}" />
                                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('job') ? 'has-error' : ''}}">
                                <label for="job" class="control-label">{{ trans('modules/users.job') }}</label>
                                <input class="form-control" name="job" type="text" id="job" value="{{ isset($user->job) ? $user->job : old('job')}}" >
                                {!! $errors->first('job', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('gender_id') ? 'has-error' : ''}}">
                                <label for="gender_id" class="control-label">{{ trans('modules/users.gender') }}</label>
                                <select class="form-control" name="gender_id" id="gender_id">
                                    @foreach($genders as $gender)
                                        <option value="{{ $gender->id }}" {{ isset($user->gender_id) && $user->gender_id==$gender->id ? 'selected' : (old('gender_id')==$gender->id?'selected':'') }}>{{ $gender->name }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('gender_id', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="{{ trans('general.save') }}">
                            </div>


                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection