@extends('layouts.app')

@section('title', ' | ' . trans('general.profile'))

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('general.profile') }}
        </h1>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @include('includes.flash_message')

                        <a href="{{ url('/admin/my-profile/edit') }}" title="{{ trans('general.edit_profile') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('general.edit_profile') }}</button></a>

                        <a href="{{ url('/admin/change-password') }}" title="{{ trans('general.change_password') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-lock" aria-hidden="true"></i> {{ trans('general.change_password') }}</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>

                                    <tr>
                                        <td>
                                            <img src="{{ getUserPhoto() }}" class="pull-right" width="200" height="200" />
                                        </td>
                                    </tr>

                                <tr>
                                    <th>{{ trans('general.id') }}</th><td>{{ $user->id }}</td>
                                </tr>
                                <tr><th> {{ trans('modules/users.name') }} </th><td> {{ $user->name }} </td></tr>
                                <tr><th> {{ trans('modules/users.email') }} </th><td> {{ $user->email }} </td></tr>
                                <tr><th> {{ trans('modules/users.office') }} </th><td> {{ isset($user->office->name)?$user->office->name:"" }} </td></tr>
                                <tr><th> {{ trans('modules/users.first_mobile') }} </th><td> {{ $user->first_mobile }} </td></tr>
                                <tr><th> {{ trans('modules/users.second_mobile') }} </th><td> {{ $user->second_mobile }} </td></tr>
                                <tr><th> {{ trans('modules/users.address') }} </th><td> {{ $user->address }} </td></tr>
                                <tr><th> {{ trans('modules/users.job') }} </th><td> {{ $user->job }} </td></tr>
                                <tr><th> {{ trans('modules/users.gender') }} </th><td> {{ isset($user->gender->name)?$user->gender->name:'' }} </td></tr>
                                <tr><th> {{ trans('modules/users.user_revenues') }} </th><td> {{ $user->revenues->sum('amount') }} </td></tr>

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
