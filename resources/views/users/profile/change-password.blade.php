@extends('layouts.app')

@section('title', ' | ' . trans('general.change_password'))

@section('content')


    <section class="content-header">
        <h1>
            {{ trans('general.change_password') }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/admin/change-password/') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                <label for="password" class="control-label">{{ trans('modules/users.password') }}</label>
                                <input class="form-control" name="password" type="password" id="password" value="{{ old('password') }}" />
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                                <label for="password_confirmation" class="control-label">{{ trans('modules/users.password_confirmation') }}</label>
                                <input class="form-control" name="password_confirmation" type="password" id="password_confirmation" value="{{ old('password_confirmation') }}" />
                                {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="{{ trans('general.save') }}">
                            </div>


                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection