<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Month;
use App\Models\Office;
use App\Models\Revenue;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RevenuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $revenues = $this->search($request, 25);

        $years = Month::select(['year'])->distinct()->pluck('year');

        $users = User::all();

        $offices = Office::all();

        return view('revenues.index', compact('revenues', 'years', 'users', 'offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $years = Month::select(['year'])->distinct()->pluck('year');

        return view('revenues.create', compact('years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateRevenues($request);

        $requestData = $request->all();

        $requestData['user_id'] = Auth::user()->id;

        if(isset(Auth::user()->office->id)) {
            $requestData['office_id'] = Auth::user()->office->id;
        }

        $requestData['created_by_id'] = Auth::user()->id;

        Revenue::create($requestData);

        return redirect('admin/revenues')->with('flash_message', trans('messages.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $revenue = Revenue::findOrFail($id);

        if(Auth::user()->is_super_admin == 0 && $revenue->user_id != Auth::user()->id) {
            abort(404);
        }

        return view('revenues.show', compact('revenue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $revenue = Revenue::findOrFail($id);

        if(Auth::user()->is_super_admin == 0 && $revenue->user_id != Auth::user()->id) {
            abort(404);
        }

        $years = Month::select(['year'])->distinct()->pluck('year');

        $selectedYear = Month::find($revenue->month_id)->year;

        return view('revenues.edit', compact('revenue', 'years', 'selectedYear'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateRevenues($request);

        $requestData = $request->all();
        
        $revenue = Revenue::findOrFail($id);

        if(isset(Auth::user()->office->id)) {
            $requestData['office_id'] = Auth::user()->office->id;
        }

        $requestData['updated_by_id'] = Auth::user()->id;

        $revenue->update($requestData);

        return redirect('admin/revenues')->with('flash_message', trans('messages.edit_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $revenue = Revenue::findOrFail($id);

        if(Auth::user()->is_super_admin == 0 && $revenue->user_id != Auth::user()->id) {
            abort(404);
        }

        Revenue::destroy($id);

        return redirect('admin/revenues')->with('flash_message', 'Revenue deleted!');
    }


    /**
     * validateRevenues
     *
     *
     * @param $request
     */
    private function validateRevenues($request)
    {
        $this->validate($request, [
           'amount' => 'required|numeric',
           'year'   => 'required',
           'month_id' => 'required',
            'description' => 'required'
        ]);
    }


    /**
     * search
     *
     *
     * @param $request
     * @param $perPage
     * @return mixed
     */
    private function search($request, $perPage)
    {
        $revenues = Revenue::whereRaw('1 = 1');

        if(!empty($request->get('id'))) {

            $revenues->where('id', $request->get('id'));
        }

        if(Auth::user()->is_super_admin == 1) {
            if (!empty($request->get('user_id'))) {

                $revenues->where('user_id', $request->get('user_id'));
            }

            if(!empty($request->get('office_id'))) {

                $revenues->where('office_id', $request->get('office_id'));
            }

        } else {
            $revenues->where('user_id', Auth::user()->id);
            $revenues->where('office_id', Auth::user()->office_id);
        }

        if(!empty($request->get('year'))) {

            $revenues->whereIn('month_id', function ($query) use ($request) {
                $query->select('id')
                    ->from('months')
                    ->where('year', $request->get('year'));
            });
        }

        if(!empty($request->get('month_id'))) {
            $revenues->where('month_id', $request->get('month_id'));
        }

        return $revenues->orderBy('id', 'DESC')->paginate($perPage);
    }
}
