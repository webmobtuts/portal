<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Month;
use App\Rules\DateWithinMonth;
use App\Rules\DateWithinYear;
use App\Rules\DuplicateMonths;
use App\Rules\MonthExists;
use App\Rules\YearExists;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MonthsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getMonthsByYear']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $months = $this->search($request, 12);

        return view('months.index', compact('months'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('months.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateMonth($request);

        $requestData = $request->all();

        foreach ($requestData['month'] as $index => $value) {

            Month::create([
                'year' => $requestData['year'],
                'month' => $value,
                'start_date' => $requestData['start_date'][$index],
                'end_date' => $requestData['end_date'][$index],
                'user_id' => Auth::user()->id
            ]);
        }

        return redirect('admin/months')->with('flash_message', trans('messages.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $month = Month::findOrFail($id);

        return view('months.show', compact('month'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $month = Month::findOrFail($id);

        return view('months.edit', compact('month'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        
        $month = Month::findOrFail($id);
        $month->update($requestData);

        return redirect('admin/months')->with('flash_message', 'Month updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Month::destroy($id);

        return redirect('admin/months')->with('flash_message', 'Month deleted!');
    }


    /**
     * getMonthsByYear
     *
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function getMonthsByYear(Request $request)
    {
        if(!$request->year)
            return;

        $months = Month::where('year', $request->year)->get();

        return response()->json(['months' => $months]);
    }


    /**
     * validateMonth
     *
     *
     * @param $request
     */
    private function validateMonth($request)
    {
        $rules = [
          'year' => ['required', 'numeric', 'min:2019', new YearExists()]
        ];

        foreach ($request->month as $index => $val) {

            $rules['month.' . $index] = ['required', 'numeric', 'min:1', 'max:12', new DuplicateMonths(array_except($request->month, $index)), new MonthExists($request->year, $request->start_date[$index], $request->end_date[$index])];

            if(isset($request->end_date[$index - 1]) && !empty($request->end_date[$index - 1])) {
                $rules['start_date.' . $index] = ['required', 'date_format:Y-m-d', 'before:end_date.'.$index, 'after:end_date.' . ($index-1), new DateWithinMonth($request->month[$index]), new DateWithinYear($request->year)];
            } else {
                $rules['start_date.' . $index] = ['required', 'date_format:Y-m-d', 'before:end_date.'.$index, new DateWithinMonth($request->month[$index]), new DateWithinYear($request->year)];
            }

            $rules['end_date.' . $index] = ['required', 'date_format:Y-m-d', 'after:start_date.' . $index, new DateWithinMonth($request->month[$index]), new DateWithinYear($request->year)];
        }

        $this->validate($request, $rules);
    }


    /**
     * search
     *
     *
     * @param $request
     * @param $perPage
     * @return mixed
     */
    private function search($request, $perPage)
    {
        $months = Month::whereRaw('1 = 1');

        if(!empty($request->get('id'))) {

            $months->where('id', $request->get('id'));
        }

        if(!empty($request->get('year'))) {

            $months->where('year', $request->get('year'));
        }

        if(!empty($request->get('month'))) {

            $months->where('month', $request->get('month'));
        }

        return $months->orderBy('id', 'ASC')->paginate($perPage);
    }
}
