<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Gender;
use App\Models\Office;
use App\Models\User;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    private $is_edit = 0;

    private $upload_dir;


    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getProfile', 'getEditProfile', 'postEditProfile', 'getChangePassword', 'postChangePassword']]);

        $this->upload_dir = public_path('uploads');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $offices = Office::all();

        $users = $this->search($request, 25);

        return view('users.index', compact('users', 'offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $offices = Office::all();

        $genders = Gender::all();

        return view('users.create', compact('offices', 'genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateUser($request);

        $requestData = $request->all();

        // check if there already other users assigned to this office
        $checkUserToOffice = User::where('office_id', $requestData['office_id'])->get();

        if(count($checkUserToOffice)) {

            return redirect()->back()->withInput()->with('flash_error', trans('messages.office_already_has_user'));
        }

        if ($request->hasFile('photo')) {

            checkDirectory("users");

            $upload = uploadFile($request, 'photo', $this->upload_dir . '/users');

            if($upload['state'] == 0) {

                return redirect()->back()->withInput()->with('flash_error', trans('messages.photo_upload_error').': ' . $upload['error']);
            }

            $requestData['photo'] = $upload['file'];
        }

        $requestData['password'] = bcrypt('12345');

        // set this flag so user can edit his password on login
        $requestData['required_password_change'] = 1;

        User::create($requestData);

        return redirect('admin/users')->with('flash_message', trans('messages.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $offices = Office::all();

        $genders = Gender::all();

        return view('users.edit', compact('user', 'offices', 'genders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->is_edit = 1;

        $this->validateUser($request, $id);

        $requestData = $request->all();

        if(isset($requestData['office_id'])) {

            // check if there already other users assigned to this office
            $checkUserToOffice = User::where('office_id', $requestData['office_id'])->where('id', '!=', $id)->get();

            if (count($checkUserToOffice)) {

                return redirect()->back()->withInput()->with('flash_error', trans('messages.office_already_has_user'));
            }
        }

        if ($request->hasFile('photo')) {

            checkDirectory("users");

            $upload = uploadFile($request, 'photo', $this->upload_dir . '/users');

            if($upload['state'] == 0) {

                return redirect()->back()->withInput()->with('flash_error', trans('messages.photo_upload_error').': ' . $upload['error']);
            }

            $requestData['photo'] = $upload['file'];
        }

        $user = User::findOrFail($id);

        $user->update($requestData);

        return redirect('admin/users')->with('flash_message', trans('messages.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user->is_super_admin == 1) {

            return redirect()->back()->with('flash_error', trans('messages.cannot_delete_super_admin'));
        }

        if($user->revenues->count() > 0) {
            return redirect()->back()->with('flash_error', trans('messages.cannot_delete_user'));
        }

        if(!empty($user->photo) && \File::exists('uploads/users/' . $user->photo)) {

            @unlink(public_path('uploads/users/' . $user->photo));
        }

        User::destroy($id);

        return redirect('admin/users')->with('flash_message', trans('messages.delete_success'));
    }


    /**
     * show user profile
     *
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile()
    {
        $user = User::findOrFail(Auth::user()->id);

        return view('users.profile.view', compact('user'));
    }

    /**
     * getEditProfile
     *
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditProfile()
    {
        $user = User::findOrFail(Auth::user()->id);

        $genders = Gender::all();

        return view('users.profile.edit', compact('user', 'genders'));
    }


    /**
     * postEditProfile
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditProfile(Request $request)
    {
        $id = Auth::user()->id;

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'photo' => 'image|mimes:jpeg,png,jpg,gif',
        ]);

        $requestData = $request->except(['_token']);

        if ($request->hasFile('photo')) {

            checkDirectory("users");

            $upload = uploadFile($request, 'photo', $this->upload_dir . '/users');

            if($upload['state'] == 0) {

                return redirect()->back()->withInput()->with('flash_error', trans('messages.photo_upload_error').': ' . $upload['error']);
            }

            $requestData['photo'] = $upload['file'];
        }

        $user = User::findOrFail($id);

        $user->update($requestData);

        return redirect('admin/my-profile')->with('flash_message', trans('messages.profile_updated'));
    }


    /**
     * getChangePassword
     *
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChangePassword()
    {
        return view('users.profile.change-password');
    }


    /**
     * postChangePassword
     *
     *
     * @param Request $request
     */
    public function postChangePassword(Request $request)
    {
        $this->validate($request, [
           'password' => 'required|min:6',
           'password_confirmation' => 'required|min:6|same:password'
        ]);

        $user = User::find(Auth::user()->id);

        $user->password = bcrypt($request->password);

        $user->required_password_change = 0;

        $user->save();

        return redirect('admin/my-profile')->with('flash_message', trans('messages.password_changed'));
    }

    /**
     * validateUser
     *
     *
     * @param $request
     * @param int $id
     */
    private function validateUser($request, $id = 0)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email' . ($this->is_edit == 1? ','.$id:''),
            'photo' => 'image|mimes:jpeg,png,jpg,gif'
        ];

        if($this->is_edit == 0 || ($this->is_edit == 1 && ($user = User::find($id)) && $user->is_super_admin == 0)) {

            $rules['office_id'] = 'required';
        }

        $this->validate($request, $rules);
    }


    /**
     * search
     *
     *
     * @param $request
     * @param $perPage
     * @return mixed
     */
    private function search($request, $perPage)
    {
        $search_keyword = $request->get('search');

        $users = User::whereRaw('1 = 1');

        if(!empty($search_keyword)) {

            $users->where(function ($query) use ($search_keyword) {
                $query->where('name', 'LIKE', "%$search_keyword%")
                    ->orWhere('email', 'LIKE', "%$search_keyword%");
            });
        }

        if(!empty($request->get('id'))) {

            $users->where('id', $request->get('id'));
        }

        if(!empty($request->get('office_id'))) {

            $users->where('office_id', $request->get('office_id'));
        }

        return $users->orderBy('id', 'DESC')->paginate($perPage);
    }
}
