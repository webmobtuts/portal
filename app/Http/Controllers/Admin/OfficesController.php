<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Office;
use Illuminate\Http\Request;

class OfficesController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $offices = $this->search($request, 25);

        return view('offices.index', compact('offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('offices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required|unique:offices,name'
        ]);

        $requestData = $request->all();
        
        Office::create($requestData);

        return redirect('admin/offices')->with('flash_message', trans('messages.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $office = Office::findOrFail($id);

        return view('offices.show', compact('office'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $office = Office::findOrFail($id);

        return view('offices.edit', compact('office'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:offices,name,'.$id
        ]);

        $requestData = $request->all();
        
        $office = Office::findOrFail($id);

        $office->update($requestData);

        return redirect('admin/offices')->with('flash_message', trans('messages.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $office = Office::find($id);

        if($office->revenues->count() > 0 || $office->users->count() > 0) {
            return redirect()->back()->with('flash_error', trans('messages.cannot_delete_office'));
        }

        Office::destroy($id);

        return redirect('admin/offices')->with('flash_message', trans('messages.delete_success'));
    }


    /**
     * search
     *
     *
     * @param $request
     * @param $perPage
     * @return mixed
     */
    private function search($request, $perPage)
    {
        $search_keyword = $request->get('search');

        $offices = Office::whereRaw('1 = 1');

        if(!empty($search_keyword)) {

            $offices->where(function ($query) use ($search_keyword) {
                $query->where('name', 'LIKE', "%$search_keyword%")
                    ->orWhere('address', 'LIKE', "%$search_keyword%");
            });
        }

        if(!empty($request->get('id'))) {

            $offices->where('id', $request->get('id'));
        }

        return $offices->orderBy('id', 'DESC')->paginate($perPage);
    }
}
