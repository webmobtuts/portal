<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'required_password_change', 'is_super_admin', 'office_id', 'photo', 'first_mobile',
        'second_mobile', 'address', 'job', 'gender_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function office()
    {
        return $this->belongsTo(Office::class, 'office_id');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, "gender_id");
    }

    public function months()
    {
        return $this->hasMany(Month::class);
    }

    public function revenues()
    {
        return $this->hasMany(Revenue::class);
    }
}
