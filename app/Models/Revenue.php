<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'revenues';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['amount', 'user_id', 'office_id', 'month_id', 'description', 'created_by_id', 'updated_by_id'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function office()
    {
        return $this->belongsTo(Office::class, 'office_id');
    }

    public function month()
    {
        return $this->belongsTo(Month::class, 'month_id');
    }
}
