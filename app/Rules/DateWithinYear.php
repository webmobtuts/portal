<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DateWithinYear implements Rule
{

    protected $year;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($year)
    {
        $this->year = $year;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!empty($this->year) && is_numeric($this->year)) {
            return date("Y", strtotime($value)) == $this->year;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.date_must_be_within_year', ['year' => $this->year]);
    }
}
