<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DateWithinMonth implements Rule
{

    protected $month;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($month)
    {
        $this->month = $month;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!empty($this->month) && is_numeric($this->month) && $this->month >= 1 && $this->month <= 12) {
            return date("n", strtotime($value)) == $this->month;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.date_must_be_within_month', ['month' => $this->month]);
    }
}
