<?php

namespace App\Rules;

use App\Models\Month;
use Illuminate\Contracts\Validation\Rule;

class MonthExists implements Rule
{

    protected $year;
    protected $startDate;
    protected $endDate;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($year, $startDate, $endDate)
    {
        $this->year = $year;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!empty($this->year) && !empty($this->startDate) && !empty($this->endDate)) {
            $checkMonth = Month::where('month', $value)
                ->where('year', $this->year)
                ->where('start_date', $this->startDate)
                ->where('end_date', $this->endDate)
                ->count();

            if ($checkMonth > 0) {

                return false;
            }

            return true;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.this_month_already_exist');
    }
}
