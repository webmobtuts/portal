<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DuplicateMonths implements Rule
{
    protected $months;

    protected $value;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($months)
    {
        $this->months = $months;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->months = array_filter($this->months);

        if(!empty($value) && count($this->months)) {
            $this->value = $value;

            return !in_array($value, $this->months);
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.this_month_duplicated', ['month' => $this->value]);
    }
}
