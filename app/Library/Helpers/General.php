<?php
/**
 * General helpers
 */


if(!function_exists('uploadFile')) {

    /**
     * upload file
     *
     *
     * @param $request
     * @param $name
     * @param string $destination
     * @return string
     */
    function uploadFile($request, $name, $destination = '')
    {
        try {
            $file = $request->file($name);

            $name = time() . '.' . $file->getClientOriginalExtension();

            if ($destination == '') {
                $destination = public_path('/uploads');
            }

            $file->move($destination, $name);

            return ['state' => 1, 'file' => $name];
        } catch (\Exception $ex) {
            return ['state' => 0, 'error' => $ex->getMessage()];
        }
    }
}

if(!function_exists('checkDirectory')) {
    /**
     * check directory exists and try to create it
     *
     *
     * @param $directory
     */
    function checkDirectory($directory)
    {
        try {
            if (!file_exists(public_path('uploads/' . $directory))) {
                mkdir(public_path('uploads/' . $directory));

                chmod(public_path('uploads/' . $directory), 0777);
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
}

if(!function_exists('getModules')) {

    function getModules()
    {
        return [
            [
                'title' => trans('sidebar.home'),
                'icon' => 'fa fa-dashboard',
                'url' => url('admin/'),
                'active_class' => \Request::segment(2) == ''?'active':'',
                'access' => true
            ],
            [
                'title' => trans('sidebar.offices'),
                'icon' => 'fa fa-building',
                'url' => url('admin/offices'),
                'active_class' => \Request::segment(2) == 'offices'?'active':'',
                'access' => \Auth::user()->is_super_admin == 1
            ],
            [
                'title' => trans('sidebar.revenues'),
                'icon' => 'fa fa-dollar',
                'active_class' => \Request::segment(2) == 'months'||\Request::segment(2) == 'revenues'?'active':'',
                'access' => true,
                'subitems' => [
                    [
                        'title' => trans('sidebar.months'),
                        'icon' => 'fa fa-calendar',
                        'url' => url('admin/months'),
                        'active_class' => \Request::segment(2) == 'months'?'active':'',
                        'access' => \Auth::user()->is_super_admin == 1
                    ],
                    [
                        'title' => trans('sidebar.revenues'),
                        'icon' => 'fa fa-dollar',
                        'url' => url('admin/revenues'),
                        'active_class' => \Request::segment(2) == 'revenues'?'active':'',
                        'access' => true
                    ]
                ]
            ],
            [
                'title' => trans('sidebar.users'),
                'icon' => 'fa fa-users',
                'url' => url('admin/users'),
                'active_class' => \Request::segment(2) == 'users'?'active':'',
                'access' => \Auth::user()->is_super_admin == 1
            ]
        ];
    }
}

if(!function_exists('getUserPhoto')) {

    function getUserPhoto($user_id = null) {

        if($user_id == null)
            $user_id = \Illuminate\Support\Facades\Auth::user()->id;

        $user = \App\Models\User::find($user_id);

        if($user->photo != "" && file_exists(public_path('uploads/users/' . $user->photo))) {

            return url('uploads/users/' . $user->photo);
        }

        return url('theme/dist/img/image_placeholder.png');
    }
}

function getOfficesCount()
{
    return \App\Models\Office::count();
}

function getUsersCount()
{
    return \App\Models\User::count();
}

function getTotalRevenues()
{
    return \App\Models\Revenue::sum('amount');
}

function getLatestRevenues($user_id = null)
{
    if($user_id) {
        return \App\Models\Revenue::where('user_id', $user_id)->latest()->paginate(5);
    }

    return \App\Models\Revenue::latest()->paginate(5);
}