<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return redirect('/admin');

});

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('/', 'HomeController@index')->name('home');

    Route::resource('/users', 'Admin\\UsersController');

    Route::resource('/offices', 'Admin\\OfficesController');

    Route::resource('/months', 'Admin\\MonthsController');

    Route::resource('/revenues', 'Admin\\RevenuesController');

    Route::get('/months-by-year', 'Admin\\MonthsController@getMonthsByYear');

    Route::get('/my-profile', 'Admin\\UsersController@getProfile');

    Route::get('/my-profile/edit', 'Admin\\UsersController@getEditProfile');

    Route::patch('/my-profile/edit', 'Admin\\UsersController@postEditProfile');

    Route::get('/change-password', 'Admin\\UsersController@getChangePassword');

    Route::patch('/change-password', 'Admin\\UsersController@postChangePassword');
});