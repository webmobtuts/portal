
$(function () {

   $("#year").on("change", function (e) {

       getMonths($(this).val(), function (response) {
           response.months.map(function (obj) {
               $("#month_id").append('<option value="' + obj.id + '" ' + ($("#month_id").attr("data-selected") == obj.id?'selected':'') + '>' + obj.month + '</option>');
           });
       });
   });

    $("#year").trigger("change");
});