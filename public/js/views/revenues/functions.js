var getMonths = function (year, callback) {

    $("#month_id").find("option").not(':first').remove();

    $.ajax({
        url: BASE_URL + '/admin/months-by-year',
        method: "GET",
        data: {year: year},
        dataType: "json",
        success: function (response) {
            callback(response);
        }
    });
}